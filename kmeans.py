import os, random, copy
import numpy as np
from sklearn.metrics import accuracy_score
from PIL import Image

#parses csv into two dimensional numpy array
def getData(fp, randomize = True):
    table = []
    with open(fp) as fh:
        for line in fh:
            line = line.strip()
            table.append(line.split(','))
    
    if randomize:
        random.shuffle(table)
    
    return np.array(table, dtype=np.float64)

#compute accuracy and confusion matrix
def printEval(testExp, predicted, classes):
    print("Accuracy: " + str(accuracy_score(testExp, predicted)))
    confMatrix = np.zeros([classes,classes], dtype=np.integer)
    for exp, pre in zip(testExp, predicted):
        confMatrix[int(exp)][int(pre)] += 1

    print(confMatrix)
    print("")

class KMeans:
    def __init__(self, nodes, minVals, maxVals):
        self.nodes = nodes
        
        #set clusters to random valid locations
        self.points = []
        for i in range(nodes):
            self.points.append([])
            for minimum, maximum in zip(minVals, maxVals):
                self.points[-1].append(random.uniform(minimum, maximum))

        self.points = np.array(self.points, dtype=np.float64)
        self.pClass = np.zeros(nodes)

    def fit(self, trainData, expected, classes):
        oldPoints = None
        clusters = None
        
        for i in range(10000):
            #assign each example to the closest cluser
            clusters = [[] for i in range(self.nodes)]
            for example in trainData:
                clusters[self.closestNode(example)].append(example)

            #move center points to mean of elements
            for i in range(self.nodes):
                if clusters[i] != []:
                    self.points[i] = np.mean(clusters[i], axis=0)
            
            #done training when no clusters are switching
            if np.array_equal(oldPoints, self.points):
                print("Convergence in " + str(i) + " iterations")
                break
            
            oldPoints = copy.deepcopy(self.points)

        #find most common occurance in each cluster for classifcation
        table = np.zeros([self.nodes, classes])
        for example, exp in zip(trainData, expected):
            index = self.closestNode(example)
            table[index][exp] += 1
        
        emptyRow = np.zeros(classes)
        emptyClusters = []
        #assign each cluster a prediction based on number of occurances
        for i in range(len(self.points)):
            if np.array_equal(emptyRow, table[i]):
                emptyClusters.append(i)
            else:
                closests =  np.argwhere(table[i]==np.amax(table[i]))
                self.pClass[i] = random.choice(closests.flatten().tolist())

        #remove empty clusters
        clusters = np.delete(clusters, emptyClusters, axis=0)
        self.points = np.delete(self.points, emptyClusters, axis=0)
        self.pClass = np.delete(self.pClass, emptyClusters, axis=0)

        print("MSE: " + str(self.mse(clusters)))
        print("MSS: " + str(self.mss()))

    def distance(self, x, y):
        return np.sqrt(np.sum((x - y)**2, axis=1))
    
    #return index of closest cluster to a new point. Break ties at random
    def closestNode(self, point):
        distances = self.distance(self.points, point)
        closests = np.argwhere(distances==np.amin(distances))
        return random.choice(closests.flatten().tolist())
       
    def predict(self, testData):
        results = []
        
        for example in testData:
            guess = self.closestNode(example)
            results.append(self.pClass[guess])

        return results

    def mse(self, clusters):
        result = 0.
        numPoints = len(self.points)
        for i in xrange(numPoints):
            distances =  self.distance(clusters[i], self.points[i])**2
            result += np.sum(distances)/float(len(clusters[i]))
        return result/float(numPoints)


    def mss(self):
        result = 0.
        numPoints = len(self.points)
        for i in xrange(numPoints):
            distances = self.distance(self.points, self.points[i])**2
            result += sum(distances)
        return result/float(numPoints * (numPoints - 1) / 2)

if __name__ == "__main__":
    folder = os.path.dirname(os.path.realpath(__file__))
    testData = getData(os.path.join(folder, "optdigits", "optdigits.test"))
    trainData = getData(os.path.join(folder, "optdigits", "optdigits.train"))
    
    #get column that has the classification
    testExp = np.array(testData[:,-1], dtype=np.integer)
    trainExp = np.array(trainData[:,-1], dtype=np.integer)
    
    #remove column that has classification. Only features are left
    testData = np.delete(testData, -1, axis=1)
    trainData = np.delete(trainData, -1, axis=1)

    for numNodes in [10,30]:
        for i in range(0,5):
            random.seed()
            #train
            features = len(testData[0])
            mins = np.full((features), 0.)
            maxs = np.full((features), 16.)
            kmeans = KMeans(numNodes, mins, maxs)
            kmeans.fit(trainData, trainExp, 10)
            print(kmeans.pClass)
            
            #test
            predicted = kmeans.predict(testData)
            printEval(testExp, predicted, 10)

            #save centers as grey scale values
            for j in range(len(kmeans.points)):
                greyScale = kmeans.points[j]/16*256
                greyScale.resize((8,8))
                img = Image.fromarray(greyScale)
                img = img.convert('RGB')
                img.save("./result_" + str(numNodes) + "_" + str(i) + "_" + str(j) + ".png")
